set number
syntax on
autocmd VimEnter * NERDTree
autocmd BufEnter * NERDTreeMirror

call plug#begin()

	Plug 'scrooloose/nerdtree'
	Plug 'scrooloose/syntastic'
	Plug 'bling/vim-airline'


call plug#end()
